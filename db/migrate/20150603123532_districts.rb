class Districts < ActiveRecord::Migration
  def up
  	   create_table :districts do |t|
	    	t.integer :s_no
	    	t.string :name
	      	t.timestamps
    	end
  end

  def down
  end
end
