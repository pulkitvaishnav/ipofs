class RemoveForeignKeyFromCaseFile < ActiveRecord::Migration
  def up
  	remove_foreign_key(:case_files, :districts)
    remove_column :case_files, :district_id
  end

  def down
  end
end
