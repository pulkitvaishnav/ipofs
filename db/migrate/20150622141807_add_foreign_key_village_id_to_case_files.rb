class AddForeignKeyVillageIdToCaseFiles < ActiveRecord::Migration
  def change
  	add_column :case_files, :village_id, :integer
  	add_foreign_key(:case_files, :villages)
  end
end
