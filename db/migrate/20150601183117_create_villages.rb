class CreateVillages < ActiveRecord::Migration
  def change
    create_table :villages do |t|
    	t.integer :s_no
    	t.string :name
    	t.string :tehsil_name
      	t.timestamps
    end
  end
end
