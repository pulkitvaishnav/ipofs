class CreateTehsils < ActiveRecord::Migration
  def change
    create_table :tehsils do |t|
    	t.integer :s_no
    	t.string :name
    	t.string :district_name
		t.timestamps
    end
  end
end
