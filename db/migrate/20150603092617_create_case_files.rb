class CreateCaseFiles < ActiveRecord::Migration
  def change
    create_table :case_files do |t|
      t.string :file_no
      t.string :title
      t.date :file_date
      t.time :file_time
      t.string :event
      t.string :deo
      t.text :remark

      t.timestamps
    end
  end
end
