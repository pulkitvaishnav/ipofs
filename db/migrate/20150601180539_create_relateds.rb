class CreateRelateds < ActiveRecord::Migration
  def change
    create_table :relateds do |t|
    	t.string :name
    	t.string :relation
    	t.string :address
    	t.integer :contact
    	t.string :guardian_name
    	t.text :remark

      t.timestamps
    end
  end
end
