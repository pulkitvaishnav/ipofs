class CreateLaws < ActiveRecord::Migration
  def change
    create_table :laws do |t|
      t.references :case_file
      t.references :act

      t.timestamps
    end
    add_index :laws, :case_file_id
    add_index :laws, :act_id
  end
end
