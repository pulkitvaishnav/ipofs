class RemoveForeignKeyFromSection < ActiveRecord::Migration
  def up
  	remove_foreign_key(:sections, :acts)
    remove_column :sections, :act_id
  end

  def down
  end
end
