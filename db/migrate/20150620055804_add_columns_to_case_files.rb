class AddColumnsToCaseFiles < ActiveRecord::Migration
  def change
    add_column :case_files, :officer_name, :string
    add_column :case_files, :designation, :string
  end
end
