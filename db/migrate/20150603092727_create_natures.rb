class CreateNatures < ActiveRecord::Migration
  def change
    create_table :natures do |t|
      t.text :nature_desc

      t.timestamps
    end
  end
end
