class CreateThanas < ActiveRecord::Migration
  def change
    create_table :thanas do |t|
    	t.integer :s_no
    	t.string :name
    	t.string :address
      	t.timestamps
    end
  end
end
