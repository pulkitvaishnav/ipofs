class CreateDistricts < ActiveRecord::Migration
  def change
    create_table :districts do |t|
    	t.integer :s_no
    	t.string :name
      	t.timestamps
    end
  end
end
