class RemoveColumnSNoFromDistricts < ActiveRecord::Migration
  def up
    remove_column :districts, :s_no
  end

  def down
    add_column :districts, :s_no, :integer
  end
end
