class AddForeignKeyToCaseFiles < ActiveRecord::Migration
  def change
    add_column :case_files, :thana_id, :integer
  	add_foreign_key(:case_files, :thanas)
  end
end
