class DropPofficers < ActiveRecord::Migration
  def up
  	remove_foreign_key(:case_files, :pofficers)
  	remove_column :case_files, :pofficer_id
  	drop_table :pofficers
  end

  def down
  end
end
