class CreatesTablePassages < ActiveRecord::Migration
  def up
  	create_table :passages do |t|
    	t.integer :case_file_id
    	t.integer :section_id

      t.timestamps
    end
    add_foreign_key(:passages, :case_files)
    add_foreign_key(:passages, :sections)
  end

  def down
  end
end
