class AddForeignKey < ActiveRecord::Migration
  def up
  	add_column :case_files, :district_id, :integer 
  	add_foreign_key(:case_files, :districts)
  	add_column :case_files, :nature_id, :integer
  	add_foreign_key(:case_files, :natures)
  	add_column :case_files, :pofficer_id, :integer
  	add_foreign_key(:case_files, :pofficers)
  	add_column :relateds, :case_file_id, :integer
  	add_foreign_key(:relateds, :case_files)
  	add_column :pofficers, :thana_id, :integer
  	add_foreign_key(:pofficers, :thanas)
  	add_column :tehsils, :district_id, :integer
  	add_foreign_key(:tehsils, :districts)
  	add_column :villages, :tehsil_id, :integer
  	add_foreign_key(:villages, :tehsils)
  end

  def down
  end
end
