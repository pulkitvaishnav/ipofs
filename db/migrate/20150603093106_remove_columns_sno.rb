class RemoveColumnsSno < ActiveRecord::Migration
  def up
  	remove_column :pofficers, :thana
  	remove_column :thanas, :s_no
  	remove_column :villages, :s_no, :tehsil_name
  	remove_column :districts, :s_no
  	remove_column :tehsils, :s_no, :district_name
  	change_column :relateds, :contact, :string
  end

  def down
  end
end
