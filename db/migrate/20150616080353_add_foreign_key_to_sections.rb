class AddForeignKeyToSections < ActiveRecord::Migration
  def change
    add_column :sections, :act_id, :integer
    add_foreign_key(:sections, :acts)
  end
end
