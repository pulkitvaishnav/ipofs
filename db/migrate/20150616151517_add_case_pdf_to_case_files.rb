class AddCasePdfToCaseFiles < ActiveRecord::Migration
  def change
    add_column :case_files, :case_pdf, :string
  end
end
