# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150622141807) do

  create_table "acts", :force => true do |t|
    t.string   "act_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "case_files", :force => true do |t|
    t.string   "file_no"
    t.string   "title"
    t.date     "file_date"
    t.time     "file_time"
    t.string   "event"
    t.string   "deo"
    t.text     "remark"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "address"
    t.integer  "nature_id"
    t.string   "case_pdf"
    t.integer  "thana_id"
    t.string   "officer_name"
    t.string   "designation"
    t.integer  "village_id"
  end

  add_index "case_files", ["nature_id"], :name => "case_files_nature_id_fk"
  add_index "case_files", ["thana_id"], :name => "case_files_thana_id_fk"
  add_index "case_files", ["village_id"], :name => "case_files_village_id_fk"

  create_table "districts", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "laws", :force => true do |t|
    t.integer  "case_file_id"
    t.integer  "act_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "laws", ["act_id"], :name => "index_laws_on_act_id"
  add_index "laws", ["case_file_id"], :name => "index_laws_on_case_file_id"

  create_table "natures", :force => true do |t|
    t.text     "nature_desc"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "passages", :force => true do |t|
    t.integer  "case_file_id"
    t.integer  "section_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "passages", ["case_file_id"], :name => "passages_case_file_id_fk"
  add_index "passages", ["section_id"], :name => "passages_section_id_fk"

  create_table "relateds", :force => true do |t|
    t.string   "name"
    t.string   "relation"
    t.string   "address"
    t.string   "contact"
    t.string   "guardian_name"
    t.text     "remark"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "case_file_id"
  end

  add_index "relateds", ["case_file_id"], :name => "relateds_case_file_id_fk"

  create_table "sections", :force => true do |t|
    t.string   "section_no"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tehsils", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "district_id"
  end

  add_index "tehsils", ["district_id"], :name => "tehsils_district_id_fk"

  create_table "thanas", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "villages", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "tehsil_id"
  end

  add_index "villages", ["tehsil_id"], :name => "villages_tehsil_id_fk"

  add_foreign_key "case_files", "natures", name: "case_files_nature_id_fk"
  add_foreign_key "case_files", "thanas", name: "case_files_thana_id_fk"
  add_foreign_key "case_files", "villages", name: "case_files_village_id_fk"

  add_foreign_key "passages", "case_files", name: "passages_case_file_id_fk"
  add_foreign_key "passages", "sections", name: "passages_section_id_fk"

  add_foreign_key "relateds", "case_files", name: "relateds_case_file_id_fk"

  add_foreign_key "tehsils", "districts", name: "tehsils_district_id_fk"

  add_foreign_key "villages", "tehsils", name: "villages_tehsil_id_fk"

end
