(function() {
  jQuery(function() {
    var tehsils, villages;
    tehsils = $('#tehsils_select').html();
    $('#districts_select').change(function() {
      var district, options;
      district = $('#districts_select :selected').text();
      options = $(tehsils).filter("optgroup[label = '" + district + "']").html();
      if (options) {
        $('#tehsils_select').html(options);
        $('#tehsils_select').prepend("<option value=''>Select a Tehsil</option>");
        return $('#tehsils_select option:first').attr("selected", "selected");
      } else {
        return $('#tehsils_select').empty();
      }
    });
    villages = $('#villages_select').html();
    return $('#tehsils_select').change(function() {
      var options, tehsil;
      tehsil = $('#tehsils_select :selected').text();
      console.log("Hello");
      options = $(villages).filter("optgroup[label = '" + tehsil + "']").html();
      if (options) {
        $('#villages_select').html(options);
        $('#villages_select').prepend("<option value=''>Select a Village</option>");
        return $('#villages_select option:first').attr("selected", "selected");
      } else {
        return $('#villages_select').empty();
      }
    });
  });

}).call(this);
