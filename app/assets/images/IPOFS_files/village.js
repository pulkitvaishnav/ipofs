(function() {
  jQuery(function() {
    var villages;
    villages = $('#villages_select').html();
    return $('#tehsils_select').change(function() {
      var options, tehsil;
      tehsil = $('#tehsils_select :selected').text();
      console.log("Hello");
      options = $(villages).filter("optgroup[label = '" + tehsil + "']").html();
      if (options) {
        return $('#villages_select').html(options);
      } else {
        return $('#villages_select').empty();
      }
    });
  });

}).call(this);
