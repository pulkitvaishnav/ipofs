jQuery ->
	tehsils = $('#tehsils_select').html()
	$('#districts_select').change ->
	    district = $('#districts_select :selected').text()
	    options = $(tehsils).filter("optgroup[label = '#{district}']").html()
	    if options
	    	$('#tehsils_select').html(options)
	    	$('#tehsils_select').prepend("<option value=''>Select a Tehsil</option>")
	    	$('#tehsils_select option:first').attr("selected", "selected");
	    else
	        $('#tehsils_select').empty()

	villages = $('#villages_select').html()
	$('#tehsils_select').change ->
	    tehsil = $('#tehsils_select :selected').text()
	    console.log("Hello")
	    options = $(villages).filter("optgroup[label = '#{tehsil}']").html()
	    if options
	    	$('#villages_select').html(options)
	    	$('#villages_select').prepend("<option value=''>Select a Village</option>")
	    	$('#villages_select option:first').attr("selected", "selected");
	    else
	        $('#villages_select').empty()