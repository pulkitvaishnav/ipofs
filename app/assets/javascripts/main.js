$(document).ready(function(){
  $('.datepicker').datepicker({
  	format: 'yyyy/mm/dd',
  	endDate: '0d'
  });
  $("#toggle1").attr("disabled", true);
  $("#toggle2").click(function() {
    $("#tabs a[href=\"#detail2\"]").tab("show");
  });
  $("#toggle3").click(function() {
    $("#tabs a[href=\"#detail1\"]").tab("show");
  });
  $("#toggle4").click(function() {
    $("#tabs a[href=\"#detail3\"]").tab("show");
  });
  $("#toggle5").click(function() {
    $("#tabs a[href=\"#detail2\"]").tab("show");
  });
  $("#toggle6").click(function() {
    $("#tabs a[href=\"#detail4\"]").tab("show");
  });
  $("#toggle7").click(function() {
    $("#tabs a[href=\"#detail3\"]").tab("show");
  });
  $("#toggle8").click(function() {
    $("#tabs a[href=\"#detail5\"]").tab("show");
  });
  $("#toggle9").click(function() {
    $("#tabs a[href=\"#detail4\"]").tab("show");
  });
  $("#toggle10").attr("disabled", true);
});
