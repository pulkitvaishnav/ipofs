class CaseFile < ActiveRecord::Base
  belongs_to :village 
  belongs_to :nature 
  belongs_to :thana
  has_many :relateds, dependent: :delete_all
  
  has_many :laws, dependent: :delete_all
  has_many :acts, through: :laws	

  has_many :passages, dependent: :delete_all
  has_many :sections, through: :passages

  attr_accessible :deo, :event, :file_date, :file_no, :file_time, :remark, :title, :address, :officer_name, :designation, :act_list, :section_list, :case_pdf, :case_pdf_cache, :remove_case_pdf, :thana_id, :nature_id, :village_id
  
  accepts_nested_attributes_for :relateds, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true  

  attr_accessible :relateds_attributes

  # File Upload
  mount_uploader :case_pdf, CaseUploader  

  def section_list
    self.sections.collect do |section|
      section.section_no
    end.join(", ")  
  end  

  def section_list=(section)
    section_no = section.split(",").collect {|s| s.strip.downcase}.uniq  
    new_or_found_sections = section_no.collect{ |num| Section.find_or_create_by_section_no(section_no: num)}
    self.sections = new_or_found_sections
  end  
  
  def act_list
    self.acts.collect do |act|
      act.act_name
    end.join(", ")  
  end  

  def act_list=(act)
    act_name = act.split(",").collect {|s| s.strip.downcase}.uniq  
    new_or_found_acts = act_name.collect{ |name| Act.find_or_create_by_act_name(act_name: name)}
    self.acts = new_or_found_acts
  end

  # Searching, sorting and filtering
  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc'},
    available_filters: [
      :sorted_by,
      :search_query,
      :with_thana_name
    ]    
  )

  scope :search_query, lambda { |query|
    return nil if query.blank?

    # condition query, parse into individual keywords
    terms = query.to_s.downcase.split(/\s+/)

    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      ('%' + e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }

    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 3
    where(
      terms.map {
        or_clauses = [
          "case_files.file_no LIKE ?",
          "case_files.title LIKE ?",
          "case_files.officer_name LIKE ?"
        ].join(' OR ')
          "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
      direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
      case sort_option.to_s
      when /^created_at_/
        # Simple sort on the created_at column.
        # Make sure to include the table name to avoid ambiguous column names.
        # Joining on other tables is quite common in Filterrific, and almost
        # every ActiveRecord table has a 'created_at' column.
        order("case_files.created_at #{ direction }")
      else
        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
      end
  }

  def self.options_for_sorted_by
      [
        ['Case File date (newest first)', 'created_at_desc'],
        ['Case File date (oldest first)', 'created_at_asc']
      ]
  end

  scope :with_thana_name, lambda { |thana_name|
    where('thanas.name = ?', thana_name).joins(:thana)
  }  

end
