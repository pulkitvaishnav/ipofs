class Section < ActiveRecord::Base
	attr_accessible :section_no
	
	has_many :passages, dependent: :delete_all
  	has_many :case_files, through: :passages
end
