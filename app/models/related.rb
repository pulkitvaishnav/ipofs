class Related < ActiveRecord::Base
	belongs_to :case_file
	attr_accessible :name, :relation, :address, :contact, :guardian_name, :remark 
end
