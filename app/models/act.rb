class Act < ActiveRecord::Base
  attr_accessible :act_name
  
  has_many :laws, dependent: :delete_all
  has_many :case_files, through: :laws
end
