class Thana < ActiveRecord::Base
	has_many :case_files, dependent: :delete_all
	attr_accessible :name, :address

	# Following fields must not remain blank
	validates :name, :address, presence: true

end
