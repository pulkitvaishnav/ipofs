class Village < ActiveRecord::Base
	belongs_to :tehsil
	has_many :case_files, dependent: :delete_all
	attr_accessible :tehsil_id, :name, :tehsil
end
