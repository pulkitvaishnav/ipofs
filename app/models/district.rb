class District < ActiveRecord::Base
	has_many :tehsils, dependent: :delete_all
	has_many :villages, through: :tehsils, dependent: :delete_all
	attr_accessible :name
	attr_accessible :tehsils_attributes
	attr_accessible :case_files_attributes
	attr_accessible :villages_attributes
	accepts_nested_attributes_for :tehsils, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true  
end
