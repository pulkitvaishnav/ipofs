class Nature < ActiveRecord::Base
	has_many :case_files, dependent: :delete_all 
  	attr_accessible :nature_desc, :nature_id
  	attr_accessible :case_files_attributes
end
