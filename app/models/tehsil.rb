class Tehsil < ActiveRecord::Base
	belongs_to :district
	has_many :villages, dependent: :delete_all
	attr_accessible :district_id, :name, :district
	attr_accessible :villages_attributes
	accepts_nested_attributes_for :villages, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true  
end
