class Passage < ActiveRecord::Base
  belongs_to :case_file
  belongs_to :section
end
